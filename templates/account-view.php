<?php
use tools\User as User;
require_once '../tools/functions.php';
require_once '../tools/User.php';

$db = \tools\db_connect(); 
session_start();
$user = User::findById($_SESSION['user_id'], $db);
?>
<div class="account-container">
	<h1>Привет, <?php if($user){ echo htmlspecialchars($user->getName()); }?>!</h1>
	<?php if($user):?>

	<div class="link-container">
		<a href="" class="logout-link">Выйти</a>
		<a href="" class="delete-link">Удалить пользователя</a>
	</div>
</div>


<?php else:
$host  = $_SERVER['HTTP_HOST'];
$uri   = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
$extra = 'auth-page.php';
header("Location: http://$host$uri/$extra");
?>
<?php endif; ?>
