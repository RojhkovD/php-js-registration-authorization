<?php
header('Content-Type: application/json; charset=UTF-8'); // for ajax response

use tools\User as User;
require_once '../tools/functions.php';
require_once '../tools/User.php';

$db = \tools\db_connect();

if($_POST['purgeSession'] == true) {

	try{
		
		session_start();
		unset($_SESSION['user_id']);
		session_unset();    
		session_destroy();

		echo json_encode(array('success' => true));

	}catch(Exception $e){

		echo json_encode(array('success' => false));

	}

}

?>