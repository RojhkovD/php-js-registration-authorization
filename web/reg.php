<?php
// header('Content-Type: application/json; charset=UTF-8');

// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
// error_reporting(E_ALL);

require_once '../tools/functions.php';
require_once '../tools/User.php';

$db = \tools\db_connect(); // returns new PDO

if ($_SERVER["REQUEST_METHOD"] == "POST") {

	$user = trim($_POST['user_name']);
	$password = $_POST['user_password'];
	$password_check = $_POST['user_password_check'];
	
}

tools\validate_reg($user, $password, $password_check, $db);

// if exit inside validate_reg not proced (no errors)
$user = new tools\User($user, $password);

//then persist it to db
$user->persist();
$host  = $_SERVER['HTTP_HOST'];
$uri   = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
$extra = 'auth-page.php';
header("Location: http://$host$uri/$extra");

?>