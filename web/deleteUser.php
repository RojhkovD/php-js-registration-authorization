<?php
header('Content-Type: application/json; charset=UTF-8'); // for ajax response

use tools\User as User;
require_once '../tools/functions.php';
require_once '../tools/User.php';

$db = \tools\db_connect();

if($_POST['deleteUser'] == true) {
	try{
		session_start();
		$user = User::findById($_SESSION['user_id'], $db);
		if($user){
			unset($_SESSION['user_id']);
			session_unset();    
			session_destroy();
			if(User::delete($user->getId(), $db)){ // static???

				echo json_encode(array('success' => true));

			}

		}else{

			echo json_encode(array('success' => false));

		}

	}catch(Exception $e){

		echo json_encode(array('success' => false));

	}
}

?>