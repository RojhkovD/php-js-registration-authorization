<?php
// header('Content-Type: application/json; charset=UTF-8');

// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
// error_reporting(E_ALL);

require_once '../tools/functions.php';

$db = \tools\db_connect(); // returns new PDO

if ($_SERVER["REQUEST_METHOD"] == "POST") {

	$user = trim($_POST['user_name']);
	$password = $_POST['user_password'];

}

$user = \tools\validate_auth($user, $password, $db);
if($user){
	//Session cookie
	session_start();

	$_SESSION['user_id'] = $user->getId(); // ????

}


$host  = $_SERVER['HTTP_HOST'];
$uri   = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
$extra = 'account.php';
header("Location: http://$host$uri/$extra");
?>