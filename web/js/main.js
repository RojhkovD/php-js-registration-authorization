(function(){
	//https://learn.javascript.ru/cookie
	function getCookie(name) {
	  var matches = document.cookie.match(new RegExp(
	    "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
	  ));
	  return matches ? decodeURIComponent(matches[1]) : undefined;
	}
	var user_id = getCookie('PHPSESSID'),
		deleteLink = document.querySelector('.delete-link'),
		logoutLink = document.querySelector('.logout-link'),
		regButton = document.querySelector('.reg-button'),
		authButton = document.querySelector('.auth-button'),
		projectFolderName = '/php-js-registration-authorization/web/';

	if(user_id){
		if(deleteLink){
			deleteLink.addEventListener('click', function(e){
				e.preventDefault();
				var xhr = new XMLHttpRequest();
				xhr.open('POST', 'deleteUser.php');
				xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
				xhr.send("deleteUser=true");
				xhr.onreadystatechange = function(){
					if(this.readyState != 4){
						return;
					}
					var res = JSON.parse(xhr.responseText);
					if(res.success === true){
						alert("Пользователь удален");
						setTimeout(function(){
							location.replace('http://' + location.hostname  + projectFolderName + "reg-page.php");
						}, 1000);
					}else{
						alert("Пользователь НЕ удален");
					}

				}
				xhr.onerror = function(){
					alert("Что то пошло не так...");
				}

			});
		}

		if(logoutLink){
			logoutLink.addEventListener('click', function(e){
				e.preventDefault();
				var xhr = new XMLHttpRequest();
				xhr.open('POST', 'purgeSession.php');
				xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
				xhr.send("purgeSession=true");
				xhr.onreadystatechange = function(){
					if(this.readyState != 4){
						return;
					}
					var res = JSON.parse(xhr.responseText);
					if(res.success === true){
						alert("Сессия удалена");
						setTimeout(function(){
							location.replace('http://' + location.hostname + projectFolderName + "auth-page.php");
						}, 1000);
					}else{
						alert("Сессия НЕ удалена");
						console.dir(res);
					}

				}
				xhr.onerror = function(){
					alert("Что то пошло не так...");
				}

			});
		}

	}
	if(regButton){
		regButton.addEventListener('click', function(e){
			e.preventDefault();

			[].forEach.call(document.querySelectorAll('p'), function(el){
				el.parentElement.removeChild(el);
			});

			// data from form
			var form = document.forms.user_reg,
				user_name = form.elements.user_name.value,
				user_password = form.elements.user_password.value,
				user_password_check = form.elements.user_password_check.value;
				debugger;
			var xhr = new XMLHttpRequest();
			xhr.open('POST', 'reg.php');
			xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
			xhr.send("user_name=" + encodeURIComponent(user_name) + "&user_password=" + encodeURIComponent(user_password) + "&user_password_check=" + encodeURIComponent(user_password_check));
			xhr.onreadystatechange = function(){
				debugger;
				if(this.readyState != 4){
					return;
				}
				var res = JSON.parse(xhr.responseText);
				if(res.error_field && res.error_cause){
					var p = document.createElement('p'),
						field = document.querySelector('input[name=' + res.error_field + ']'),
						parent = field.parentElement;
					p.innerHTML = res.error_cause;
					parent.insertBefore(p, field.nextElementSibling);
				}else if(res.success === true){
					location.replace('http://' + location.hostname + projectFolderName + "auth-page.php");
				}else{
					alert("Регистрация не состоялась");
				}
				
			}
			xhr.onerror = function(){
				alert("Что то пошло не так...");
			}

		});
	}

	if(authButton){
		authButton.addEventListener('click', function(e){
			e.preventDefault();

			[].forEach.call(document.querySelectorAll('p'), function(el){
				el.parentElement.removeChild(el);
			});

			// data from form
			var form = document.forms.user_auth,
				user_name = form.elements.user_name.value,
				user_password = form.elements.user_password.value;

			var xhr = new XMLHttpRequest();
			xhr.open('POST', 'auth.php');
			xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
			xhr.send("user_name=" + user_name + "&user_password=" + user_password);
			xhr.onreadystatechange = function(){
				if(this.readyState != 4){
					return;
				}
				debugger;
				var res = JSON.parse(xhr.responseText);
				if(res.error_field && res.error_cause){
					var p = document.createElement('p'),
						field = document.querySelector('input[name=' + res.error_field + ']'),
						parent = field.parentElement;
					p.innerHTML = res.error_cause;
					parent.insertBefore(p, field.nextElementSibling);
				}else if(res.success === true){
					location.replace('http://' + location.hostname + projectFolderName + "account.php");
				}else{
					alert("Регистрация не состоялась");
				}
				
			}
			xhr.onerror = function(){
				alert("Что то пошло не так...");
			}

		});
	}


})();