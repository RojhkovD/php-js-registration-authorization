> _1 Поместить проект в корень веб сервера._
>
> > `cd /var/www/html`
> >
> > `git clone https://RojhkovD@bitbucket.org/RojhkovD/php-js-registration-authorization.git`
>
> _2 Установить пользователя и пароль к базе данных в функции db_connect(), которая находится в web/tools/funсtions.php._
>
> > `$dsn = 'mysql:host=localhost;dbname=DBNAME';`
> >
> > `$user = 'USER';`
> >
> >	`$password = 'PASSWORD';`
>
> _3 Создать базу из дампа в корне проекта - db.sql_
> > `mysql -uroot -ppassword`
> >
> > `create database DBNAME;`
> >
> > `use DBNAME`
> >
> > `source /var/www/html/php-js-registration-authorization/db.sql`
>
> _4 Открыть в браузере `localhost/php-js-registration-authorization/web`_