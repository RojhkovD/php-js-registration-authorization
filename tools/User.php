<?php
namespace tools;

require_once '../tools/functions.php';

class User
{
	private $id;

	private $name;

	private $password;

	public $db;

	public function setId($id)
	{

		$this->id = $id;

	}


	public function getId()
	{

		return $this->id;

	}

	public function setName($name)
	{

		$this->name = $name;

	}

	public function getName()
	{

		return $this->name;

	}

	public function setPassword($password)
	{

		$this->password = $password;

	}

	public function getPassword()
	{

		return $this->password;

	}

	public static function findById($id, $db) 
	{

		$stm = $db->prepare('SELECT * FROM users u WHERE u.id = :id');
		$stm->execute(array('id' => $id));
		$row = $stm->fetch();
		if($row != null){

			$user = new User(null, null);
			$user->setName($row['name']);
			$user->setPassword($row['password']);
			$user->setId($row['id']);
			return $user;

		}else{

			return false;

		}


	}

	// find and convert db row to User object
	public static function findByName($name, $db) 
	{

		$stm = $db->prepare('SELECT * FROM users u WHERE u.name = :name');
		$stm->execute(array('name' => $name));
		$row = $stm->fetch();
		if($row != null){

			$user = new User(null, null);
			$user->setName($row['name']);
			$user->setPassword($row['password']);
			$user->setId($row['id']);
			return $user;

		}else{

			return false;

		}

	}

	public function persist()
	{
		//check if already exists in db and need update
		$oldUser = User::findByName($this->name, $this->db);
		if($oldUser != null){

			$stm = $this->db->prepare('UPDATE users u SET u.name = :username, u.password = :password WHERE u.id = :id');
			$stm->execute(array('username' => $this->name, 'password' => $this->password, 'id' => $oldUser->id));

		}else{

			$stm = $this->db->prepare('INSERT INTO users (name, password) VALUES(:username, :password)');
			$stm->execute(array('username' => $this->name, 'password' => $this->password));

		}

		//set id as we persist it to db ?????
		$this->setId(User::findByName($this->name,  $this->db)->getId());
	}

	public static function delete($id, $db)
	{
		// if exists in db
		if($id != null){

			$stm = $db->prepare('DELETE FROM users WHERE users.id = :id');
			return $stm->execute(array('id' => $id));

		}

		return false;

	}

	public function verify($input)
	{	

		return password_verify($input, $this->getPassword());

	}

	public function getHash($password)
	{

		return password_hash($password, PASSWORD_BCRYPT);

	}

	public function __construct($name, $password)
	{

		$this->db = \tools\db_connect();

		if($name != null && $password != null){

			$password = $this->getHash($password);
			$this->setName($name);
			$this->setPassword($password);

		}

	}
		
}
?>