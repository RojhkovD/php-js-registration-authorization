<?php
namespace tools;

class FormError
{
	public $error_field;

	public $error_cause;

	public function __construct($field, $cause)
	{
		$this->error_field = $field;
		$this->error_cause = $cause;
	}
	
}
?>