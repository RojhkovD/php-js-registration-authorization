<?php
namespace tools;

require_once '../tools/FormError.php';
require_once '../tools/User.php';

function db_connect(){

	$dsn = 'mysql:host=localhost;dbname=dbName';
	$user = 'user';
	$password = '';
	return new \PDO($dsn, $user, $password);

}

function validate_reg($username, $password, $password_check, $db){

	// validate username
	if(!(strlen($username) <= 100 && strlen($username) >= 3)){

		$err = new FormError('user_name', 'Username length should be from 3 to 100 characters');
		echo json_encode($err);
		exit;

	}else{

		if(!preg_match('/^[a-zA-Z\p{Cyrillic}0-9_]{3,100}$/u', $username)){

			$err = new FormError('user_name', 'A username can only contain alphanumeric characters with underscore');
			echo json_encode($err);	
			exit;

		}else{

			$sth = $db->prepare('SELECT * FROM users u WHERE u.name = :username'); 
			$sth->execute(array('username' => $username));
			$rows = $sth->fetchAll();
			if(!empty($rows)){

				$err = new FormError('user_name', 'Username exists');
				echo json_encode($err);
				exit;

			}else{
				//validate password
				if(!(strlen($password) <= 100 && strlen($password) >= 3)){

					$err = new FormError('user_password', 'Password length should be from 3 to 100 characters');
					echo json_encode($err);
					exit;

				}else{

					if($password !== $password_check){

						$err = new FormError('user_password_check', 'Password doesn\'t match');
						echo json_encode($err);
						exit;

					}else{

						//success
						echo json_encode(array('success' => true));
						ob_flush();
				        flush();
					}
					
				}

			}

		}

	}

}

function validate_auth($username, $password, $db){

	if(!(strlen($username) <= 100 && strlen($username) >= 3)){

		$err = new FormError('user_name', 'Incorrect input');
		echo json_encode($err);
		exit;

	}

	if(!(strlen($password) <= 100 && strlen($password) >= 3)){

		$err = new FormError('user_password', 'Incorrect input');
		echo json_encode($err);
		exit;

	}

	$existing = User::findByName($username, $db);
	if($existing != null && $existing->verify($password)){
		
		//success, return user
		echo json_encode(array('success' => true));
		ob_flush();
		flush();
		return $existing;

	}else{

		$err = new FormError('user_name', 'Username doesn\'t exist or password doesn\'t match');
		echo json_encode($err);
		exit;

	}
}

?>
